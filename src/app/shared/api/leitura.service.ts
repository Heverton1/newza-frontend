import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { GenericService } from './generic.service';
import { Leitura } from '../../leitura/leitura';
import { Observable } from 'rxjs';
import { TipoMedidorEnum } from 'src/app/leitura/tipo-medidor-enum';
import { ValorMedidorConsumo } from 'src/app/leitura/valor-medidor-consumo';

@Injectable()
export class LeituraService extends GenericService<Leitura> {

  constructor(http: HttpClient){
      super(http);
      super.base = 'leitura';
  }

  public buscarIdMedidor(id: number): Observable<Leitura[]> {
      return this.http.get<Leitura[]>(`${this.api}/${this.base}/medidor/${id}`, {headers: this.header});
  }

  public buscarUltimaLeitura(id: number): Observable<Leitura> {
      return this.http.get<Leitura>(`${this.api}/${this.base}/ultimaleitura/${id}`, {headers: this.header});
  }

  public medirLeituraAgua(valor: number): Promise<number> {
    const promise = new Promise<number>((resolve, reject) => {
      const body = JSON.stringify({valor: valor, tipo: TipoMedidorEnum.AGUA});
      this.http.post<number>(`${this.api}/${this.base}/medirLeitura`, body, {headers: this.header })
      .toPromise()
      .then((res: any) => {
        // Success
        resolve(res);
      }, err => {
          // Error
          reject(0);
        }
      )
    });
    return promise;
  }

  public medirLeituraLuz(valor: number): Promise<number> {
      const promise = new Promise<number>((resolve, reject) => {
        const body = JSON.stringify({valor: valor, tipo: TipoMedidorEnum.LUZ});
      this.http.post<number>(`${this.api}/${this.base}/medirLeitura`, body, {headers: this.header })
        .toPromise()
        .then((res: any) => {
          // Success
          resolve(res);
        }, err => {
            // Error
            reject(0);
          }
        )
      });
      return promise;
  }

  public realizarCalculo(leituras: Array<Leitura>): Array<ValorMedidorConsumo>{
    const valoresConsumos = new Array<ValorMedidorConsumo>();

    leituras.sort((a: Leitura, b: Leitura) => {
      if (a.dataleitura > b.dataleitura) { return 1; }
      if (a.dataleitura < b.dataleitura) { return -1; }
      return 0;
    });

    let leituraAnterior: Leitura;
    let leituraAtual: Leitura;

    for (let i = 0; i < leituras.length; i++){
      if (i === 0) {
        leituraAtual = leituras[i];
        leituraAnterior = new Leitura();
        leituraAnterior.numeroleitura = 0;
        const total = leituraAtual.numeroleitura;
        valoresConsumos.push(this.prepararCalculo(leituraAtual, leituraAnterior, total));
      } else {
        const j = i - 1;
        leituraAnterior = leituras[j];
        leituraAtual = leituras[i];
        const total = leituraAtual.numeroleitura - leituraAnterior.numeroleitura;
        valoresConsumos.push(this.prepararCalculo(leituraAtual, leituraAnterior, total));
      }
    }

    return valoresConsumos;
  }
  
  private prepararCalculo(leituraAtual: Leitura, leituraAnterior: Leitura, total: number): ValorMedidorConsumo {
    const valorConsumo = new ValorMedidorConsumo();

    valorConsumo.numeroAtual = leituraAtual.numeroleitura;
    valorConsumo.numeroAnterior = leituraAnterior.numeroleitura;

    valorConsumo.dataLeituraAtual = leituraAtual.dataleitura;
    valorConsumo.dataLeituraAnterir = leituraAnterior.dataleitura;

    valorConsumo.medidor = leituraAtual.medidorConsumo;
    valorConsumo.quantidade = (total <= 0 ) ? 0 : total;

    if (leituraAtual.medidorConsumo.tipoMedidorConsumo.id === TipoMedidorEnum.LUZ) {
      valorConsumo.unidadeMedida = 'Kw';
      this.medirLeituraLuz(total).then(result => { valorConsumo.valor = result});
    } else if (leituraAtual.medidorConsumo.tipoMedidorConsumo.id === TipoMedidorEnum.AGUA) {
      valorConsumo.unidadeMedida = 'M³';
      this.medirLeituraAgua(total).then(result => { valorConsumo.valor = result});
    }

    return valorConsumo;
  }
}
