import { HttpBackend, HttpClient, HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Login } from '../../login/login';
import { GenericService } from '../api/generic.service';
import { MensagemComponente } from '../components/mensagem.component';
import { JwtRequest } from './jwt-request';
import { UsuarioLogado } from './usuario-logado';

@Injectable()
export class AuthService extends GenericService<JwtRequest> {

    private httpClient: HttpClient;
    public jwtr = new JwtRequest();

    constructor(handler: HttpBackend, http: HttpClient, private router: Router, private mensagem: MensagemComponente){
        super(http);
        super.base = 'autenticacao';
        this.httpClient = new HttpClient(handler);
    }

    public isAuthenticated(): boolean {
        // Check whether the token is expired and return
        // true or false
        // const decodedToken = this.jwtHelper.decodeToken(token);
        // const expirationDate = this.jwtHelper.getTokenExpirationDate(token);
        // const isExpired = this.jwtHelper.isTokenExpired(token);
        // Criar serviço para verificar se já expirou o token

        // https://medium.com/@ryanchenkie_40935/angular-authentication-using-the-http-client-and-http-interceptors-2f9d1540eb8
        console.log(UsuarioLogado.getToken() !== null);
        return UsuarioLogado.getToken() !== null; // !this.jwtHelper.isTokenExpired(token);
    }
    /**
     * Utilizar essa para fazer o primeiro login
     * https://stackoverflow.com/questions/46469349/how-to-make-an-angular-module-to-ignore-http-interceptor-added-in-a-core-module
     */
    public buscar(dados: JwtRequest): Observable<JwtRequest> {
        const body = JSON.stringify(dados);
        return this.httpClient.post<JwtRequest>(`${this.api}/${this.base}`, body, {headers: this.header});
    }

    public logout(): void {
        this.clearStorage();
        this.router.navigate(['']);
    }

    public clearStorage(): void {
        UsuarioLogado.clearStorage();
    }

}
