import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { LoadingController, ToastController } from '@ionic/angular';
import { environment } from 'src/environments/environment';

@Injectable()
export class MensagemComponente {
    private debugger: boolean = !environment.production;

    constructor(private toastController: ToastController, private loadingService: LoadingController){}

    async presentToastErroMensagem(mensagem: string, rep?: HttpErrorResponse) {
      const toast = await this.toastController.create({
        color: 'danger',
        message: mensagem,
        duration: 2000,
        buttons: [{
            text: 'Close',
            role: 'cancel',
            handler: () => {}
          }
        ]
      });
      toast.present();
      if (this.debugger) {
        console.log('Erro: ', rep);
      }
    }

    async presentToastErro(rep: HttpErrorResponse) {
      const toast = await this.toastController.create({
        color: 'danger',
        message: rep.error.message + ' - ' + rep.error.cause.message,
        duration: 2000,
        buttons: [{
            text: 'Close',
            role: 'cancel',
            handler: () => {}
          }
        ]
      });
      toast.present();
      if (this.debugger) {
        console.log('Erro: ', rep);
      }
    }

    async presentToastSucess(mensagem: string, result?: any) {
      const toast = await this.toastController.create({
        message: mensagem,
        duration: 2000,
        buttons: [{
            text: 'Close',
            role: 'cancel',
            handler: () => {}
          }]
      });
      toast.present();

      if (this.debugger) {
        console.log('Sucesso: ', result);
      }
  }

  async presentLoading(duracao?: number): Promise<void> {
    return await this.loadingService.create({
      message: 'Aguarde...',
      duration: duracao,
    }).then(res => {
      res.present();
    });
  }

  async closePresentLoading(): Promise<void> {
    while (await this.loadingService.getTop() !== undefined) {
      await this.loadingService.dismiss();
    }
  }
}