import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { LeituraService } from '../../shared/api/leitura.service';
import { UsuarioLogado } from '../../shared/auth/usuario-logado';
import { Leitura } from '../leitura';
import { MedidorConsumo } from '../medidor-consumo';
import { TipoMedidorEnum } from '../tipo-medidor-enum';
import { ValorMedidorConsumo } from '../valor-medidor-consumo';

@Component({
  selector: 'app-visualiza-leitura-modal',
  templateUrl: './visualiza-leitura-modal.component.html',
  styleUrls: ['visualiza-leitura-modal.component.scss']
})
export class VisualizaLeituraModalComponent implements OnInit {

  @Input() item: MedidorConsumo;
  leituras = new Array<Leitura>();
  valoresConsumos = new Array<ValorMedidorConsumo>();
  isAdministrador = UsuarioLogado.getUsuarioLogadoPerfilAdministrador();

  constructor(private service: LeituraService) {
  }
  
  ngOnInit(): void {
    this.service.buscarIdMedidor(this.item.id).subscribe(result => {
      this.leituras = result;
      this.valoresConsumos = this.service.realizarCalculo(this.leituras);

      this.valoresConsumos.sort((a: ValorMedidorConsumo, b: ValorMedidorConsumo) => {
        if (a.dataLeituraAtual < b.dataLeituraAtual) { return 1; }
        if (a.dataLeituraAtual > b.dataLeituraAtual) { return -1; }
        return 0;
      });

    });
  }

  editar(valoresConsumos: ValorMedidorConsumo): void {
  }

  excluir(valoresConsumos: ValorMedidorConsumo): void {
  }

}
